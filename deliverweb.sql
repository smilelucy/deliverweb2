create database fcts;

-- 送餐員
create table delivery_person (
    `s_num` int(10) UNSIGNED NOT NULL COMMENT '外送員序號',
    `dp_nickname` varbinary(300) DEFAULT NULL COMMENT '暱稱',
    `dp_img` LONGBLOB DEFAULT NULL COMMENT '照片',
    `dp_reason` varbinary(300) DEFAULT NULL COMMENT '送餐理由',
    `dp01` varbinary(300) NOT NULL COMMENT '中文姓',
    `dp02` varbinary(300) NOT NULL COMMENT '中文名',
    `dp_experience` varbinary(300) DEFAULT NULL COMMENT '經驗',
    `status` int(1) DEFAULT '0'
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ;
-- 案主
create table client (
    `s_num` int(10) UNSIGNED NOT NULL COMMENT '案主序號',
    `ct_name` varbinary(300) NOT NULL COMMENT '案主姓名',
    `ct_address` varbinary(300) DEFAULT NULL COMMENT '地址',
    `ct_lon` double DEFAULT NULL COMMENT '案家經度',
    `ct_lat` double DEFAULT NULL COMMENT '案家緯度',
    `status` int(1) DEFAULT '1'
);

-- 案主路徑
create table client_route(
    `s_num` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `ct_s_num` int(10) UNSIGNED NOT NULL,
    `ct_order` int(5) NOT NULL ,
    `reh_s_num` int(10) UNSIGNED NOT NULL,
    PRIMARY KEY (`s_num`) USING BTREE
);

-- 路徑
create table route (
    `s_num` int(10) UNSIGNED NOT NULL COMMENT '序號',
    `reh_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '路線名稱',
    `reh_category` int(1) DEFAULT NULL COMMENT '路線類別(1=山線，2=海線，3=屯線)',
    `reh_time` int(1) DEFAULT NULL COMMENT '路線適用時段(1=午餐/午晚餐，2=晚餐)',
    `status` int(1) DEFAULT '0'
);

-- 送餐打卡
create table punch (
    `s_num` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '打卡序號',
    `dp_s_num` int(10) UNSIGNED NOT NULL COMMENT '外送員編號',
    `reh_s_num` int(10) NOT NULL DEFAULT '0' COMMENT '路徑編號',
    `ph_time` datetime NOT NULL COMMENT '打卡時間',
    `ph_inorout` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '打卡型態(1=簽到;2=簽退)',
    `ph_lon` double DEFAULT NULL COMMENT '打卡經度',
    `ph_lat` double DEFAULT NULL COMMENT '打卡緯度',
    `ph_wifi` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '打卡方式(1=有網路，2=無網路)',
    `ct_s_num` int(10) UNSIGNED NOT NULL COMMENT '案主編號',
    PRIMARY KEY (`s_num`) USING BTREE
);

-- gps
create table gps (
    `s_num` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `reh_s_num` int(10) UNSIGNED NOT NULL COMMENT '路徑編號',
    `gps_time` datetime NOT NULL COMMENT '紀錄時間',
    `gps_lon` double DEFAULT NULL COMMENT 'gps經度',
    `gps_lat` double DEFAULT NULL COMMENT 'gps緯度',
    `dp_s_num` int(10) UNSIGNED NOT NULL COMMENT '外送員編號',
    PRIMARY KEY (`s_num`) USING BTREE
);

CREATE TABLE `daily_shipment` (
  `dp_s_num` int(10) NOT NULL COMMENT 'tw_delivery_person.s_num',
  `ct_s_num` int(10) UNSIGNED NOT NULL COMMENT 'tw_clients.s_num',
  `sec_s_num` int(10) UNSIGNED NOT NULL COMMENT 'tw_service_case.s_num',
  `reh_s_num` int(4) NOT NULL COMMENT 'tw_route_h.s_num',
  `ct_name` varbinary(300) NOT NULL COMMENT '案主姓名(tw_clients.ct01+tw_clients.ct02)',
  `dys01` date NOT NULL COMMENT '送餐日期',
  `dys02` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '餐別',
  `dys03` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '餐點名稱',
  `dys04` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '特殊內容',
  `dys05` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '代餐(Y=有,N=沒有)',
  `dys05_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '代餐種類',
  `dys06` int(4) NOT NULL COMMENT '是否異動',
  `reh_name` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '路線代碼',
  `ct_order` int(1) NOT NULL COMMENT '送餐順序',
  `dys09` int(1) NOT NULL COMMENT '送餐時間',
  `dys10` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '是否出餐',
  `dys11` int(1) NOT NULL DEFAULT '2' COMMENT '配送狀況(1=送達，2=未送達)',
  `dys12` datetime DEFAULT NULL COMMENT '送達時間',
  `dys13` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '自費'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='每日配送單資料';

-- 紀錄距離table
CREATE TABLE `distance` (
    `s_num` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `reh_s_num` int(4) NOT NULL ,
    `lon` double DEFAULT NULL ,
    `lat` double DEFAULT NULL ,
    `km` double DEFAULT NULL ,
    `insert_time` datetime NOT NULL,
  PRIMARY KEY (`s_num`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ;