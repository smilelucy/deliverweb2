const mariadb = require('mariadb');

// here we create a new connection pool
const pool = mariadb.createPool({
  host: "127.0.0.1",
  // local
  user: "root",  
  password: "password",
   port:'3308', // local
  database: "fcts", // for 測試資料
  // database: "fcts_real" // for 正是資料
  // user: "lucy", // server
  // password: "880101",
  // port: "3306", // server
  // database: "fcts"
});
// here we are exposing the ability to creating new connections
module.exports={
  connection: function(){
      // return new Promise(function(resolve,reject){
      //   pool.getConnection().then(function(connection){
          
      //     resolve(connection);
      //   }).catch(function(error){
      //     reject(error);
      //   });
        
      // });
      return new Promise(function(resolve,reject){
        try{
          try {
            pool.totalConnections();
  
          } catch(e) {
            pool.getConnection();
          }
          pool.getConnection().then(function(connection){
            resolve(connection);
          }).catch(function(error){
            reject(error);
          });
        } catch (e) {
          reject(e);
        }
      });
    }
  }

