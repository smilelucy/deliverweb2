const mariadb = require('mariadb');

// here we create a new connection pool
const pool = mariadb.createPool({
  host: "127.0.0.1",
  // local
  user: "root", 
  password: "password",
  port:'3308', // local
  database: "fcts"
  // server
  // user: "lucy", 
  // password: "880101", 
  // port: "3306",
  // database: "fcts_test"
});

// here we are exposing the ability to creating new connections
module.exports={
  connection: function(){
      return new Promise(function(resolve,reject){
        pool.getConnection().then(function(connection){
          resolve(connection);
        }).catch(function(error){
          reject(error);
        });
      });
    }
  }

