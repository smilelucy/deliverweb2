module.exports = {
    insert_gps: (db, lat, lon, reh_s_num) => {
        return new Promise(async(res, rej) => {
          const cmd = `insert into gps(lat, lon, `
          + `reh_s_num, gps_time) values($1, $2, $3, now());`;
          const params = [lat, lon, reh_s_num];
          try {
            const {rowCount} = await db.query(cmd, params);
            res(rowCount === 1);
          } catch (e){
            rej(e);
          }
        });
      },
    truncate_daily_shipment: (db) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `truncate daily_shipment; `;
          // params = [reh_s_num];
          const result = await db.query(cmd);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
   
    insert_daily_shipment: (db, dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13) => {
      return new Promise(async(res, rej) => {
        try {
          await db.query('BEGIN');
          cmd = `insert into daily_shipment (dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
          params = [dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13];
          const result = await db.query(cmd, params);
          console.log('hihi');
          console.log(result);
          await db.query('COMMIT');

          res(result);
          
        } catch (e) {
          await db.query('ROLLBACK');
          rej(e);
        }
      });
    },
    get_route_name: (db, reh_s_num) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `select reh_name, count(distinct(ct_name)) as count from daily_shipment where reh_s_num = ? and dys10 = 'Y'; `;
          params = [reh_s_num];
          const result = await db.query(cmd, reh_s_num);
          // console.log('result');
          // console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
    get_all_home_position: (db, reh_s_num) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `select ct_lon, ct_lat, c.ct_name, d.dys03, ct_s_num from client as c left join daily_shipment as d on c.ct_name = d.ct_name where d.reh_s_num = ? and dys10 = 'Y' ; `;
          params = [reh_s_num];
          const result = await db.query(cmd, reh_s_num);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
    get_deliver_count: (db, reh_s_num, nowDate, nextDate ) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `select count from delivery_count where reh_s_num = ? and insert_time between ? and ?; `;
          params = [reh_s_num, nowDate, nextDate ];
          const result = await db.query(cmd, params);
          // console.log('resultCCC');
          // console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
    get_ct_name: (db, ct_s_num) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `select  ct_name from client where s_num = ?; `;
          params = [ct_s_num];
          const result = await db.query(cmd, params);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
    get_punch_time: (db, ct_s_num, nowDate, nextDate) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `select ph_time from punch where ct_s_num = ? and ph_time between ? and ?; `;
          params = [ct_s_num, nowDate, nextDate];
          const result = await db.query(cmd, params);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
    get_ct_food_name: (db, ct_s_num) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `select  dys03 from daily_shipment where ct_s_num = ?; `;
          params = [ct_s_num];
          const result = await db.query(cmd, params);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },     
    get_already_deliver: (db, reh_s_num,nowDate, nextDate) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `SELECT distinct(ct_s_num) from punch where reh_s_num = ? and ph_time between ? and ?; `;
          params = [reh_s_num,nowDate, nextDate];
          const result = await db.query(cmd, params);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },    
    get_gps: (db, reh_s_num,nowDate, nextDate) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `SELECT gps_lon, gps_lat from gps where reh_s_num = ? and gps_time between ? and ? ; `;
          params = [reh_s_num,nowDate, nextDate];
          const result = await db.query(cmd, params);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },      
    get_total_km: (db, nowDate, nextDate) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `SELECT sum(distinct(km)) as sum from distance where insert_time between ? and ? ; `;
          params = [nowDate, nextDate];
          const result = await db.query(cmd, params);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },      
    get_total_meal_count: (db, nowDate, nextDate) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `SELECT count(distinct(ct_s_num)) as count from punch where ph_time between ? and ? ; `;
          params = [nowDate, nextDate];
          const result = await db.query(cmd, params);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },      
    get_gps_list: (db, reh_s_num, nowDate, nextDate) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `select gps from gps_list where reh_s_num = ? and time between ? and ?; `;
          params = [reh_s_num, nowDate, nextDate];
          const result = await db.query(cmd, params);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },      
    insert_gps_list: (db, reh_s_num, time, gpsArray) => {
      return new Promise(async(res, rej) => {
        try {
          await db.query('BEGIN');
          // cmd = `insert into delivery_person (s_num, dp_nickname, dp_img, dp_reason, dp01, dp02, dp_experience) values(?, ?, ?, ?, ?, ?, ?)`;
          cmd = `INSERT INTO gps_list (gps, reh_s_num, time)  VALUES (?, ?, ?);`
          params = [gpsArray, reh_s_num, time];
          const result = await db.query(cmd, params);
          // console.log('hihi');
          // console.log(result);
          await db.query('COMMIT');

          res(result);
          
        } catch (e) {
          await db.query('ROLLBACK');
          rej(e);
        }
      });
    },   
  
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    insert_delivery_person: (db, s_num, dp_nickname, dp_img, dp_reason, dp01, dp02, dp_experience) => {
      return new Promise(async(res, rej) => {
        try {
          await db.query('BEGIN');
          cmd = `insert into delivery_person (s_num, dp_nickname, dp_img, dp_reason, dp01, dp02, dp_experience) values(?, ?, ?, ?, ?, ?, ?)`;
          params = [s_num, dp_nickname, dp_img, dp_reason, dp01, dp02, dp_experience];
          const result = await db.query(cmd, params);
          console.log('hihi');
          console.log(result);
          await db.query('COMMIT');

          res(result);
          
        } catch (e) {
          await db.query('ROLLBACK');
          rej(e);
        }
      });
    },   
    delete_delivery_person: (db, s_num) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `delete from delivery_person where s_num = ?; `;
          params = [s_num];
          const result = await db.query(cmd, params);
          console.log('result');
          console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
    insert_client: (db, s_num, ct_name, ct_address, ct_lon, ct_lat, status) => {
      return new Promise(async(res, rej) => {
        try {
          await db.query('BEGIN');
          cmd = `insert into client (s_num, ct_name, ct_address, ct_lon, ct_lat, status) values(?, ?, ?, ?, ?, ?)`;
          params = [s_num, ct_name, ct_address, ct_lon, ct_lat, status];
          const result = await db.query(cmd, params);
          console.log('hihi');
          console.log(result);
          await db.query('COMMIT');

          res(result);
          
        } catch (e) {
          await db.query('ROLLBACK');
          rej(e);
        }
      });
    },
    update_client: (db, s_num, status) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `update client set status = ? where s_num = ?; `;
          params = [status, s_num];
          const result = await db.query(cmd, params);
          //console.log('result');
          //console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },      
    delete_client: (db, s_num) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `delete from client where s_num = ?; `;
          params = [s_num];
          const result = await db.query(cmd, params);
          console.log('result');
          console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
    insert_client_route: (db, ct_s_num, ct_order, reh_s_num) => {
      return new Promise(async(res, rej) => {
        try {
          await db.query('BEGIN');
          cmd = `insert into client_route (ct_s_num, ct_order, reh_s_num) values(?, ?, ?)`;
          params = [ct_s_num, ct_order, reh_s_num];
          const result = await db.query(cmd, params);
          console.log('hihi');
          console.log(result);
          await db.query('COMMIT');

          res(result);
          
        } catch (e) {
          await db.query('ROLLBACK');
          rej(e);
        }
      });
    },   
    delete_client_route: (db, s_num) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `delete from client_route where reh_s_num = ?; `;
          params = [s_num];
          const result = await db.query(cmd, params);
          console.log('result');
          console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
    get_deliver_info: (db, nowDate, nextDate) => {
      return new Promise(async(res, rej) => {
        try {
          // cmd = `select reh_name, count(distinct(ct_name)) as count from daily_shipment where reh_s_num = ? and dys10 = 'Y'; `;
          // cmd = `select dp.s_num as snum, CONCAT(dp01, dp02) as name,(select count(distinct(ct_name)) from daily_shipment as ds where dys10 = 'Y' and ds.dp_s_num = dp.s_num) as foodCount from delivery_person as dp;`;
          // cmd = `select dp.s_num as snum, CONCAT(dp01, dp02) as name,(select count(distinct(ct_name)) as foodCount from daily_shipment as ds where dys10 = 'Y' and ds.dp_s_num = dp.s_num) as foodCount, (select count(distinct(ct_s_num)) from punch as p where p.dp_s_num = dp.s_num and ph_time > ? and ph_time < ?) as alreadyFood  from delivery_person as dp;`;
          // cmd = `select dp.s_num as snum, CONCAT(dp01, dp02) as name,(select count(distinct(ct_name)) as foodCount from daily_shipment as ds where dys10 = 'Y' and ds.dp_s_num = dp.s_num) as foodCount, 
          // (select count(distinct(ct_s_num)) from punch as p where p.dp_s_num = dp.s_num and ph_time > ? and ph_time < ?) as alreadyFood , 
          // (select round(km, 2) from distance as d where d.dp_s_num = dp.s_num) as km from delivery_person as dp;`;
          // cmd = `select dp.s_num as snum,reh_name , CONCAT(dp01, dp02) as name, count(distinct(ct_name)) as foodCount, (select count(distinct(ct_s_num)) from punch as p where p.reh_s_num = ds.reh_s_num and ph_time > ? and ph_time < ?) as alreadyFood, (select round(km, 2) from distance as d where d.reh_s_num = ds.reh_s_num) as km from daily_shipment as ds left join delivery_person as dp on ds.dp_s_num = dp.s_num group by reh_s_num;`;
          cmd = `select dp.s_num as snum,reh_name , CONCAT(dp01, dp02) as name, count(distinct(ct_name)) as foodCount, (select count(distinct(ct_s_num)) from punch as p where p.reh_s_num = ds.reh_s_num and ph_time > ? and ph_time < ?) as alreadyFood from daily_shipment as ds left join delivery_person as dp on ds.dp_s_num = dp.s_num where dys10 = 'Y' group by reh_s_num;`;
          params = [nowDate, nextDate];
          const result = await db.query(cmd, params);
          // console.log('result');
          // console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
    get_all_route: (db) => {
      return new Promise(async(res, rej) => {
        try {
          cmd = `select distinct(reh_name), CONCAT(dp01, dp02) as name, reh_s_num from daily_shipment as ds left join delivery_person as dp on ds.dp_s_num = dp.s_num; `;
          // params = [reh_s_num, nowDate, nextDate ];
          const result = await db.query(cmd);
          // console.log('resultCCC');
          // console.log(result);
          res(result);
        } catch (e) {
          rej(e);
        }
      });
    },   
}

