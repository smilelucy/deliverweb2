const router = require('express').Router();
const con = require('../model/db');
const conTest = require('../model/db_test');
const map = require('../model/map');
//var multer  = require('multer')
//var upload = multer()
var BodyParser = require( 'body-parser' );
router.use( BodyParser() );

// const mariadb = require('mariadb');
// const pool = mariadb.createPool({host:'127.0.0.1', user: 'hsuan', password:'880101',port: '3308', database:'fcts'}); // local
// const pool = mariadb.createPool({host:'127.0.0.1', user: 'root', password:'', database:'fcts'}); // server
const { body, validationResult } = require('express-validator');
/*
 * insert gps
 * @Param: id, pwd
 * @Return: true/false
 */
router.post('/api_test',  async(req, res) => {
    const formatError = validationResult(req);
    if(!formatError.isEmpty()) {
      return res.json({ error: `格式錯誤` });
    }
    // let db;
    // try {
    //   db = await con.connection();
    // } catch (e) {
    //   return res.json({ error: `系統錯誤` });
    // }
    try {
      //const {TEST} = req.query;
      // const success = await map.insert_gps(db, lat, lon, reh_s_num);
      //console.log(req.body);
      console.log(req.body.TEST);
      return res.json('success' + req.body.TEST);
    } catch (e) {
      console.log(`[ERROR][ACCOUNT UPDATE PASSWORD]: ${new Date().toLocaleString()}, ${e}`);
      return res.json({error: `系統錯誤` + e});
    } finally {
      // db.release();
    }
  });

  /*
 * daily shipment
 * @Param: id, pwd
 * @Return: true/false
 */
router.post('/daily_shipment',  async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` });
  }
  try {
     //const {data} = req.body;
    //console.log(data[0]);
    //const formData = req.body;
    //console.log('formdata', formData);
    // const {data} = req.body;
    //const {dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13} = JSON.stringify(req.body);
    // const success = await map.insert_gps(db, lat, lon, reh_s_num);
    //console.log(req.body);
    console.log(req.body[0]);
    // console.log(req.body[0].dp_s_num);
    console.log(req.body.length);
    await map.truncate_daily_shipment(db);
    for(let i = 0; i < req.body.length; i++) {
      
      console.log('QQQ');
      console.log("dailyshipment",req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
      await map.insert_daily_shipment(db, req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
    }
    // console.log(data);
    //console.log(dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13);
    //console.log(JSON.stringify(req.body.dp_s_num[0]));
    return res.json('success' + req.body.dp_s_num);

  
  } catch (e) {
    console.log(`[ERROR][DAILY SHIPMENT]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    // db.release();
  }
});




 /*
 * get_note_info
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
 router.post('/get_route_name', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {reh_s_num} = req.body;
    console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_route_name(db, reh_s_num);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET ROUTE NAME]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤`});
  } finally {
    db.release();
  }
});


  /*
 * get_note_info
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
router.post('/get_route_all_client', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {reh_s_num} = req.body;
    console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_route_all_client(db, reh_s_num);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET ROUTE ALL CLIENT]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤`});
  } finally {
    db.release();
  }
});

 /*
 * get_note_info
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
 router.post('/get_all_home_position', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {reh_s_num} = req.body;
    console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_all_home_position(db, reh_s_num);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET ROUTE NAME]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤`});
  } finally {
    db.release();
  }
});

router.post('/get_deliver_count', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {reh_s_num,nowDate, nextDate } = req.body;
    console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_deliver_count(db, reh_s_num, nowDate, nextDate );
    // console.log('CCC');
    // console.log(name);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET ROUTE NAME]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤`});
  } finally {
    db.release();
  }
});

router.post('/get_all_route', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    // const {reh_s_num,nowDate, nextDate } = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const data = await map.get_all_route(db);
    // console.log('CCC');
    // console.log(name);
    return res.json(data);
  } catch (e) {
    console.log(`[ERROR][GET ALL ROUTE]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤`});
  } finally {
    db.release();
  }
});


 /*
 * get_note_info
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
 router.post('/get_ct_name', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {ct_s_num} = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_ct_name(db, ct_s_num);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET CLIENT NAME]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});

/*
 * get_note_info
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
router.post('/get_punch_time', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {ct_s_num, nowDate, nextDate} = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_punch_time(db, ct_s_num, nowDate, nextDate);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET PUNCH TIME]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});

 /*
 * get_note_info
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
 router.post('/get_ct_food_name', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {ct_s_num} = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_ct_food_name(db, ct_s_num);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET CLIENT NAME]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});
    
 /*
 * get_note_info
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
 router.post('/get_already_deliver', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {reh_s_num,nowDate, nextDate} = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_already_deliver(db, reh_s_num,nowDate, nextDate);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET ALREADY DELIVER]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});

/*
 * get_note_info
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
router.post('/get_gps', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {reh_s_num,nowDate, nextDate} = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_gps(db, reh_s_num,nowDate, nextDate);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET GPS]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});

/*
 * get_total_km
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
router.post('/get_total_km', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {nowDate, nextDate} = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_total_km(db, nowDate, nextDate);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET TOTAL KM]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});

/*
 * get_total_km
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
router.post('/get_total_meal_count', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {nowDate, nextDate} = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_total_meal_count(db, nowDate, nextDate);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET TOTAL MEAL COUNT]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});

/*
 * insert gps list
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
router.post('/insert_gps_list', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {reh_s_num, time, gpsArray} = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.insert_gps_list(db, reh_s_num, time, gpsArray);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][INSERT GPS LIST]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});

/*
 * get gps list
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
router.post('/get_gps_list', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {reh_s_num, nowDate, nextDate} = req.body;
    // console.log(reh_s_num);
    // const memberNo = await account.get_no(db, id);
    const name = await map.get_gps_list(db, reh_s_num, nowDate, nextDate);
    return res.json(name);
  } catch (e) {
    console.log(`[ERROR][GET GPS LIST]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});
  
/*
 * get_deliver_info
 * @Param: id, pwd, name, phone, email, address, identification
 * @Return: true/false
 */
router.post('/get_deliver_info', [
  // authorization,
], async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` + e });
  }
  try {
    const {nowDate, nextDate} = req.body;
    const data = await map.get_deliver_info(db, nowDate, nextDate);
    return res.json(data);
  } catch (e) {
    console.log(`[ERROR][GET DELIVER INFO]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    db.release();
  }
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`
/* for正式DB API */
 /*
 * delivery_person
 * @Param: id, pwd
 * @Return: true/false
 */
 router.post('/delivery_person',  async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    console.log(formatError);
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` });
  }
  try {
    //console.log(req.json());
    console.log(req.body);
    console.log(req.body.type);
    console.log(req.body.dp_reason);
    //console.log(req.body[0]);
    // console.log(req.body[0].dp_s_num);
    //console.log(req.body.length);
    
    //for(let i = 0; i < req.body.length; i++) {
      if(req.body.type == 'add') { // 新增
        await map.insert_delivery_person(db, req.body.s_num, req.body.dp_nickname, req.body.dp_img, req.body.dp_reason, req.body.dp01, req.body.dp02, req.body.dp_experience);
      } else if(req.body.type == 'upd') { // 修改
        // 先把那比原本的資料刪除再新增
        await map.delete_delivery_person(db, req.body.s_num);
        // 再把新的資料匯入
        await map.insert_delivery_person(db, req.body.s_num, req.body.dp_nickname, req.body.dp_img, req.body.dp_reason, req.body.dp01, req.body.dp02, req.body.dp_experience);
      } else { // 刪除
        await map.delete_delivery_person(db, req.body.s_num);
      }
      // console.log('QQQ');
      // console.log(req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
      
    //}
    // console.log(data);
    //console.log(dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13);
    //console.log(JSON.stringify(req.body.dp_s_num[0]));
    return res.json('success /delivery_person');

  
  } catch (e) {
    console.log(`[ERROR][DELIVERY PERSON]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    // db.release();
  }
});

/*
 * client
 * @Param: id, pwd
 * @Return: true/false
 */
router.post('/client',  async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` });
  }
  try {
    console.log('body');
    console.log(req.body);
    console.log(req.body.type);
    //console.log(req.body[0].type);
    // console.log(req.body[0].dp_s_num);
    //console.log(req.body.length);
    //console.log(req.body[0].length);    

    //for(let i = 0; i < req.body.length; i++) {
      if(req.body.type == 'add') { // 新增
        await map.insert_client(db, req.body.s_num, req.body.ct_name, req.body.ct_address, req.body.ct_lon, req.body.ct_lat, req.body.status);
      } else if(req.body.type == 'upd') { // 修改
        // 先把那比原本的資料刪除再新增
        await map.delete_client(db, req.body.s_num);
        // 再把新的資料匯入
        await map.insert_client(db, req.body.s_num, req.body.ct_name, req.body.ct_address, req.body.ct_lon, req.body.ct_lat, req.body.status);
      } else if(req.body.type == 'stop') { // 停復餐，停餐 0/ 復餐 1
        await map.update_client(db, req.body.s_num, req.body.status);
      } else { // 刪除
        await map.delete_client(db, req.body.s_num);
      }
      // console.log('QQQ');
      // console.log(req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
      
    //}
    // console.log(data);
    //console.log(dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13);
    //console.log(JSON.stringify(req.body.dp_s_num[0]));
    return res.json('success /client');

  
  } catch (e) {
    console.log(`[ERROR][CLIENT]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    // db.release();
  }
});

/*
 * client route
 * @Param: id, pwd
 * @Return: true/false
 */
router.post('/client_route',  async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await con.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` });
  }
  try {

    console.log(req.body);
    console.log(req.body[0]);
    console.log(req.body.route.s_num);
    console.log(req.body.client_route.length);
    console.log(req.body.client_route[0]);
    // console.log(req.body[0].dp_s_num);
    console.log(req.body.length);
    await map.delete_client_route(db, req.body.route.s_num);
    for(let i = 0; i < req.body.client_route.length; i++) {
      //await map.delete_client_route(db, req.body[i].s_num);
        // 再把新的資料匯入
      await map.insert_client_route(db, req.body.client_route[i].ct_s_num, req.body.client_route[i].ct_order, req.body.client_route[i].reh_s_num);

      // console.log('QQQ');
      // console.log(req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
      
    }
    // console.log(data);
    //console.log(dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13);
    //console.log(JSON.stringify(req.body.dp_s_num[0]));
    return res.json('success /client_route');

  
  } catch (e) {
    console.log(`[ERROR][CLIENT ROUTE]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    // db.release();
  }
});

/* for 測試DB API */

/*
 * delivery_person_test
 * @Param: id, pwd
 * @Return: true/false
 */
router.post('/delivery_person_test',  async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    console.log(formatError);
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await conTest.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` });
  }
  try {
    //console.log(req.json());
    console.log(req.body);
    console.log(req.body.type);
    console.log(req.body.dp_reason);
    //console.log(req.body[0]);
    // console.log(req.body[0].dp_s_num);
    //console.log(req.body.length);
    
    //for(let i = 0; i < req.body.length; i++) {
      if(req.body.type == 'add') { // 新增
        await map.insert_delivery_person(db, req.body.s_num, req.body.dp_nickname, req.body.dp_img, req.body.dp_reason, req.body.dp01, req.body.dp02, req.body.dp_experience);
      } else if(req.body.type == 'upd') { // 修改
        // 先把那比原本的資料刪除再新增
        await map.delete_delivery_person(db, req.body.s_num);
        // 再把新的資料匯入
        await map.insert_delivery_person(db, req.body.s_num, req.body.dp_nickname, req.body.dp_img, req.body.dp_reason, req.body.dp01, req.body.dp02, req.body.dp_experience);
      } else { // 刪除
        await map.delete_delivery_person(db, req.body.s_num);
      }
      // console.log('QQQ');
      // console.log(req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
      
    //}
    // console.log(data);
    //console.log(dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13);
    //console.log(JSON.stringify(req.body.dp_s_num[0]));
    return res.json('success /delivery_person');

  
  } catch (e) {
    console.log(`[ERROR][DELIVERY PERSON]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    // db.release();
  }
});

/*
 * client
 * @Param: id, pwd
 * @Return: true/false
 */
router.post('/client_test',  async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await conTest.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` });
  }
  try {
    console.log('body');
    console.log(req.body);
    console.log(req.body.type);
    //console.log(req.body[0].type);
    // console.log(req.body[0].dp_s_num);
    //console.log(req.body.length);
    //console.log(req.body[0].length);    

    //for(let i = 0; i < req.body.length; i++) {
      if(req.body.type == 'add') { // 新增
        await map.insert_client(db, req.body.s_num, req.body.ct_name, req.body.ct_address, req.body.ct_lon, req.body.ct_lat, req.body.status);
      } else if(req.body.type == 'upd') { // 修改
        // 先把那比原本的資料刪除再新增
        await map.delete_client(db, req.body.s_num);
        // 再把新的資料匯入
        await map.insert_client(db, req.body.s_num, req.body.ct_name, req.body.ct_address, req.body.ct_lon, req.body.ct_lat, req.body.status);
      } else if(req.body.type == 'stop') { // 停復餐，停餐 0/ 復餐 1
        await map.update_client(db, req.body.s_num, req.body.status);
      } else { // 刪除
        await map.delete_client(db, req.body.s_num);
      }
      // console.log('QQQ');
      // console.log(req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
      
    //}
    // console.log(data);
    //console.log(dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13);
    //console.log(JSON.stringify(req.body.dp_s_num[0]));
    return res.json('success /client');

  
  } catch (e) {
    console.log(`[ERROR][CLIENT]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    // db.release();
  }
});

/*
 * client route
 * @Param: id, pwd
 * @Return: true/false
 */
router.post('/client_route_test',  async(req, res) => {
  const formatError = validationResult(req);
  if(!formatError.isEmpty()) {
    return res.json({ error: `格式錯誤` });
  }
  let db;
  try {
    db = await conTest.connection();
  } catch (e) {
    return res.json({ error: `系統錯誤` });
  }
  try {

    console.log(req.body);
    console.log(req.body[0]);
    console.log(req.body.route.s_num);
    console.log(req.body.client_route.length);
    console.log(req.body.client_route[0]);
    // console.log(req.body[0].dp_s_num);
    console.log(req.body.length);
    await map.delete_client_route(db, req.body.route.s_num);
    for(let i = 0; i < req.body.client_route.length; i++) {
      //await map.delete_client_route(db, req.body[i].s_num);
        // 再把新的資料匯入
      await map.insert_client_route(db, req.body.client_route[i].ct_s_num, req.body.client_route[i].ct_order, req.body.client_route[i].reh_s_num);

      // console.log('QQQ');
      // console.log(req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
      
    }
    // console.log(data);
    //console.log(dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13);
    //console.log(JSON.stringify(req.body.dp_s_num[0]));
    return res.json('success /client_route');

  
  } catch (e) {
    console.log(`[ERROR][CLIENT ROUTE]: ${new Date().toLocaleString()}, ${e}`);
    return res.json({error: `系統錯誤` + e});
  } finally {
    // db.release();
  }
});

  /*
 * daily shipment
 * @Param: id, pwd
 * @Return: true/false
 */
  router.post('/daily_shipment_test',  async(req, res) => {
    const formatError = validationResult(req);
    if(!formatError.isEmpty()) {
      return res.json({ error: `格式錯誤` });
    }
    let db;
    try {
      db = await conTest.connection();
    } catch (e) {
      return res.json({ error: `系統錯誤` });
    }
    try {
       //const {data} = req.body;
      //console.log(data[0]);
      //const formData = req.body;
      //console.log('formdata', formData);
      // const {data} = req.body;
      //const {dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13} = JSON.stringify(req.body);
      // const success = await map.insert_gps(db, lat, lon, reh_s_num);
      //console.log(req.body);
      console.log(req.body[0]);
      // console.log(req.body[0].dp_s_num);
      console.log(req.body.length);
      await map.truncate_daily_shipment(db);
      for(let i = 0; i < req.body.length; i++) {
        
        console.log('QQQ');
        console.log(req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
        await map.insert_daily_shipment(db, req.body[i].dp_s_num, req.body[i].ct_s_num, req.body[i].sec_s_num, req.body[i].reh_s_num, req.body[i].ct_name, req.body[i].dys01, req.body[i].dys02, req.body[i].dys03, req.body[i].dys04, req.body[i].dys05, req.body[i].dys05_type, req.body[i].dys06, req.body[i].reh_name, req.body[i].ct_order, req.body[i].dys09, req.body[i].dys10, req.body[i].dys11, req.body[i].dys12, req.body[i].dys13);
      }
      // console.log(data);
      //console.log(dp_s_num, ct_s_num, sec_s_num, reh_s_num, ct_name, dys01, dys02, dys03, dys04, dys05,dys05_type, dys06, reh_name, ct_order, dys09, dys10, dys11, dys12, dys13);
      //console.log(JSON.stringify(req.body.dp_s_num[0]));
      return res.json('success' + req.body.dp_s_num);
  
    
    } catch (e) {
      console.log(`[ERROR][DAILY SHIPMENT]: ${new Date().toLocaleString()}, ${e}`);
      return res.json({error: `系統錯誤` + e});
    } finally {
      // db.release();
    }
  });

module.exports = router;
