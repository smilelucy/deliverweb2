const path = require('path');
const router = require('express').Router();
const PAGE = ['index','info']; // 所有的頁面

// root page(login)
router.get('/', function (req, res) {
    // res.cookie('XSRF-TOKEN', req.csrfToken());
    res.render('index');
  });


PAGE.forEach((p) => {
    router.get(`/${p}`, function (req, res) {  
        // res.cookie('XSRF-TOKEN', req.csrfToken());
        res.render(p);
    });
});

  module.exports = router;